# README #
The objective of this project is to port a long-established remote computing system, Virtual Network Computing (VNC), to a web application housed entirely within a browser.  Our implementation requires no plug-in installation nor does it run on Flash or Java.  It builds upon existing web infrastructures and integrates seamlessly with existing VNC servers.  We utilize a network of proxy servers to improve our application’s security, performance and scalability.  Furthermore, our network infrastructure reroutes communication providing natural traversal of NATs and firewalls. 


### What is this repository for? ###

* Project updates and source control for graduate project

### To-do: ###
* Reorganize this repository





### Demo: ###
[https://www.youtube.com/watch?v=_IQ1qfvXIHU](https://www.youtube.com/watch?v=_IQ1qfvXIHU)

### Done ###



### Who do I talk to? ###

* quang.vo31@gmail.com
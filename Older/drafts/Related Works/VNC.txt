VNC

Virtual Network Computing(VNC) is a desktop sharing system to graphically control remote computers based on the Remote Frame Buffer protocol(RFB).  VNC records keyboard and mouse clicks and send them back along with a graphical image of the screen over a network.  VNC is a platform independent remote desktop system.  